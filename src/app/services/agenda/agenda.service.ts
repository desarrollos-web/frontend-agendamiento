import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class AgendaService {

	public headers: any;
	public url = environment.apiRestUrl;

	constructor(
		public http: HttpClient
	){
		this.headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
	}

	listarTecnicos() {
		return this.http.get(this.url + 'tecnicos', { headers: this.headers });
	}
	
	consultarAgenda(fecha: any, tecnico: any) {
		const params = '?fecha=' + fecha + '&tecnico=' + tecnico;
			
		return this.http.get(this.url + 'horasFijasDisponiblesTecnico' + params, { headers: this.headers });
	}
	
	registrarAgenda(agenda: any) {
		let params = '?nombres='+ agenda.nombres 
					+ '&apellidos='+ agenda.apellidos 
					+ '&tipoDocumento='+ agenda.tipoDocumento 
					+ '&documento='+ agenda.documento 
					+ '&contacto='+ agenda.contacto 
					+ '&correo='+ agenda.correo 
					+ '&placa='+ agenda.placa 
					+ '&modelo='+ agenda.modelo 
					+ '&year='+ agenda.year 
					+ '&fecha='+ agenda.fecha 
					+ '&servicio='+ agenda.servicio 
					+ '&hora='+ agenda.hora 
					+ '&observacion='+ agenda.observacion
					+ '&tecnico='+ agenda.tecnico;

		return this.http.post(this.url + 'registrarAgenda' + params, { headers: this.headers });
	}

}

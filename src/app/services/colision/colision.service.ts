import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ColisionService {

	public headers: any;
	public url = environment.apiRestUrlColision;

	constructor(
		public _http: HttpClient
	){
		this.headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
	}
	
	listarEstadoProcesos(placa) {
		const params = '?placa=' + placa;
		return this._http.post(this.url + 'listarEstadoProceso' + params, { headers: this.headers });
	}
}

import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

declare var $: any;

@Injectable({
	providedIn: 'root'
})
export class SweetAlertService {

	public resp: boolean;

	constructor() { }


	alertInput(titulo, textoHtml, textoBtnOK, textoBtnCancelar) {
		swal({
			title: titulo,
			html: '<div class="form-group">' +
			'<label class="text-dark" for="FormControlLabel">' + textoHtml + '</label>' +
			'<input id="input-field" placeholder="Observaciones..." type="text" class="form-control" />' +
			'</div>',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: textoBtnOK,
			cancelButtonText: textoBtnCancelar,
			buttonsStyling: false
		}).then((result) => {
			if (result.value) {} else {}

		}).catch(swal.noop);
	}


	alertConfirmacion() {
		swal({
			title: '¿Esta seguro de eliminar?',
			text: "Tenga en cuenta que esta acción no se podrá revertir",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Si, eliminar!',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
		}).catch(swal.noop);
	}

	alertGeneral(titulo: string = "Bien!", texto: any, textoBtn: string = "OK", tipo) {
		swal({
			title: titulo,
			text: texto,
			buttonsStyling: false,
			confirmButtonClass: "btn btn-primary",
			confirmButtonText: textoBtn,
			type: tipo			
		}).catch(swal.noop)
	}

	showNotification(from: any, align: any, type: any, icon: any, message: any, timer) {
		// const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];


		$.notify({
			icon: icon,
			message: message,
		}, {
			type: type,
			timer: timer,
			placement: {
			from: from,
			align: align
			},
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">' +
			'<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">	<i class="material-icons">close</i></button>' +
			'<i class="material-icons" data-notify="icon">warning</i> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
			'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
			'</div>'
		});
	}
}

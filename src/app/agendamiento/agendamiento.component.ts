import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AgendaService } from '../services/agenda/agenda.service';
import { SweetAlertService } from '../services/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-agendamiento',
	templateUrl: './agendamiento.component.html',
	styleUrls: ['./agendamiento.component.css']
})
export class AgendamientoComponent implements OnInit {
	tipoDocumento: string[];
	servicios: string[];

	serviciosListado = [
		{ value: 'Limpieza de motor', viewValue: 'Limpieza de motor' },
		{ value: 'Alineación', viewValue: 'Alineación' },
		{ value: 'Cambio de aceite', viewValue: 'Cambio de aceite' },
		{ value: 'Revisión de motor', viewValue: 'Revisión de motor' },
	];

	tiposDocumentos = [
		{ value: 'CC', viewValue: 'Cédula de ciudadania' },
		{ value: 'CE', viewValue: 'Cédula de extranjería' },
		{ value: 'PA', viewValue: 'Pasaporte' },
		{ value: 'TI', viewValue: 'Tarjeta de Identidad' },
		{ value: 'Nit', viewValue: 'Nit' },
		{ value: 'O', viewValue: 'Otro' },
	];
	
	modelosListado = [
		{ value: 'SPARK GT', viewValue: 'SPARK GT' },
		{ value: 'SPARK GT ACTIV', viewValue: 'SPARK GT ACTIV' },
		{ value: 'BEAT', viewValue: 'BEAT' },
		{ value: 'NUEVO ONIX', viewValue: 'NUEVO ONIX' },
		{ value: 'ONIX ACTIV', viewValue: 'ONIX ACTIV' },
		{ value: 'ONIX', viewValue: 'ONIX' },
		{ value: 'TRACKER', viewValue: 'TRACKER' },
		{ value: 'CAPTIVA', viewValue: 'CAPTIVA' },
		{ value: 'EQUINOX', viewValue: 'EQUINOX' },
		{ value: 'TRAVERSE', viewValue: 'TRAVERSE' },
		{ value: 'BLAZER', viewValue: 'BLAZER' },
		{ value: 'EQUINOX', viewValue: 'EQUINOX' },
		{ value: 'TRAILBLAZER', viewValue: 'TRAILBLAZER' },
		{ value: 'DMAX', viewValue: 'DMAX' },
		{ value: 'COLORADO', viewValue: 'COLORADO' },
		{ value: 'N300 PASAJEROS', viewValue: 'N300 PASAJEROS' },
		{ value: 'N300 CARGO', viewValue: 'N300 CARGO' },
		{ value: 'CHEVYTAXI', viewValue: 'CHEVYTAXI' },
	];

	aniosModeloListado = [];

	public consultarAgenda: any = {
		tecnico: null,
		nombre: null,
		fechaCita: null,
		horaCita: null
	};
	
	public registroAgenda: any = {
		nombres: null,
		apellidos: null,
		tipoDocumento: null,
		documento: null,
		contacto: null,
		correo: null,
		placa: null,
		modelo: null,
		year: null,
		fecha: null,
		servicio: null,
		hora: null,
		observacion: null,
		tecnico: null,
	};

	public agendaForm: FormGroup;
	public fechaTecnicoForm: FormGroup;
	public tecnicos: any;
	public horasDisponibles: any;
	public formVisibleRegistro = false;
	public messangeAgenda: any;
	public mostrarHoras = false;
	public status: string;
	public anioActual: Date;
	public loading = false;
	public check: any;

	constructor(
		private _agendaServices: AgendaService,
		private _sweetService: SweetAlertService
	){
		$("#terminosCondiciones").attr('checked', false);
	}

	ngOnInit(): void {
		this.loading = true;
		this.fechaTecnicoForm = new FormGroup({});
		this.agendaForm = new FormGroup({
			nombres: new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{1,50}$")]),
			apellidos: new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{1,50}$")]),
			tipoDocumento: new FormControl(null, Validators.required),
			documento: new FormControl(null, [Validators.required, Validators.pattern("^[0-9]{1,20}$")]),
			contacto: new FormControl(null, [Validators.required, Validators.pattern("^[0-9]{1,50}$")]),
			correo: new FormControl(null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			placa: new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-Z]{3}[0-9]{3}$")]),
			modelo: new FormControl(null, Validators.required),
			year: new FormControl(null, Validators.required),
			servicio: new FormControl(null, Validators.required),
			observacion: new FormControl(null, [Validators.pattern("^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ._%+-]{1,140}$")]),
			terminosCondiciones: new FormControl(null, Validators.required)
		});
		
		this.listar();
		this.generarAnioModelo();
	}

	prepararDatosAgenda() {
		this.registroAgenda.nombres = this.agendaForm.value.nombres;
		this.registroAgenda.apellidos = this.agendaForm.value.apellidos;
		this.registroAgenda.tipoDocumento = this.agendaForm.value.tipoDocumento;
		this.registroAgenda.documento = this.agendaForm.value.documento;
		this.registroAgenda.contacto = this.agendaForm.value.contacto;
		this.registroAgenda.correo = this.agendaForm.value.correo;
		this.registroAgenda.placa = this.agendaForm.value.placa;
		this.registroAgenda.modelo = this.agendaForm.value.modelo;
		this.registroAgenda.year = this.agendaForm.value.year;
		this.registroAgenda.fecha = this.formatoFecha(this.consultarAgenda.fechaCita);
		this.registroAgenda.servicio = this.agendaForm.value.servicio;
		this.registroAgenda.hora = this.consultarAgenda.horaCita.id;
		this.registroAgenda.observacion = this.agendaForm.value.observacion;
		this.registroAgenda.tecnico = this.consultarAgenda.tecnico.id;
	}

	async onSubmitAgendar() {
		this.loading = true;
		this.prepararDatosAgenda();

		if( this.validarDatosAgenda() ) {
			await new Promise((resolve, reject) => {
				this._agendaServices.registrarAgenda(this.registroAgenda).subscribe(
				(response: any) => {
					this.loading = false;
					console.log(response);

					if (response.status == "success" && response.code == 200) {
						this.loading = false;
						this.agendaForm.reset();
						this.check = false;
						this.limpiarCamposAgenda();
						this._sweetService.alertGeneral('Agendamiento correcto',
							response.data.correoMsg,
							'Ok',
							'success'
						);
					}
				}),
				(error: any) => {
					this.loading = false;
					console.log("[REGISTRO DE AGENDAMIENTO]: ", error);
					reject(error);
				};
			}).catch((error: any) => {
				this.loading = false;
				console.log(error);
				throw error;
			});
		} else {
			this.loading = false;
			this._sweetService.showNotification(
				"top",
				"right",
				"danger",
				"warning",
				"Todos los campos son requeridos",
				2000
			);
		}
	}

	validarDatosAgenda() {
		return this.registroAgenda.nombres && this.registroAgenda.apellidos && 
		this.registroAgenda.tipoDocumento && this.registroAgenda.documento && 
		this.registroAgenda.contacto && this.registroAgenda.correo && 
		this.registroAgenda.placa && this.registroAgenda.modelo && 
		this.registroAgenda.year && this.registroAgenda.fecha && 
		this.registroAgenda.servicio && this.registroAgenda.hora && 
		this.registroAgenda.tecnico && this.check;
	}

	async listar() {
		let tecnicos: any;
		await new Promise((resolve, reject) => {
			this._agendaServices.listarTecnicos().subscribe(
			(response: any) => {
				if (response.status == "success" && response.code == 200) {
					this.loading = false;
					this.tecnicos = response.data;
					console.log(this.tecnicos);
				}
			}),
			(error: any) => {
				this.loading = false;
				console.log("[ERROR DE LISTADO DE TECNICOS]: ", error);
				reject(error);
			};
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	seleccionarTecnico(tecnico): void {
		this.consultarAgenda.tecnico = tecnico;
		this.consultarAgenda.nombre = tecnico.nombre;
	}

	async consultarDisponibilidad() {
		if(this.consultarAgenda.fechaCita && this.consultarAgenda.tecnico) {
			if (this.validacionFechaDeAgenda(this.consultarAgenda.fechaCita)) {
				this.loading = true;

				await new Promise((resolve, reject) => {
					this._agendaServices.consultarAgenda(this.formatoFecha(this.consultarAgenda.fechaCita), this.consultarAgenda.tecnico.id).subscribe(
					(response: any) => {
						this.loading = false;
						this.horasDisponibles = response.data.horas_disponibles;
						console.log(this.horasDisponibles);

						if (this.horasDisponibles.length > 0) {
							this.mostrarHoras = true;
							this.formVisibleRegistro = false;

						} else {
							this._sweetService.alertGeneral(
								'No existe horas disponibles',
								'El técnico que acaba de seleccionar no posee horas disponibles para esta fecha. Por seleccione otra fecha y/o técnico',
								'Ok',
								'warning'
							);
							this.mostrarHoras = false;
							this.formVisibleRegistro = false;
						}
					}),
					(error: any) => {
						this.loading = false;
						console.log("[ERROR DE LISTADO DE TECNICOS]: ", error);
						reject(error);
					};
					
				}).catch((error: any) => {
					console.log(error);
					throw error;
				});
			} else {
				this._sweetService.showNotification(
					"top",
					"right",
					"danger",
					"alarm",
					"La fecha seleccionada debe ser superior a la fecha actual.",
					2000
				);
				this.mostrarHoras = false;
				this.formVisibleRegistro = false;
			}
		}
		
	}

	seleccionarHora(hora,visible): void {
		this.consultarAgenda.horaCita = hora;
		this.formVisibleRegistro = visible;
		console.log(this.consultarAgenda.horaCita);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;

		if (e1 && e2) {
			result = e1 === e2;
		}

		return result;
	}

	formatoFecha(date: Date): string{
		const dia = date.getDate();
		const mes = date.getMonth()+1;
		const anio = date.getFullYear();

		return `${anio}-${mes}-${dia}`;
	}

	generarAnioModelo(): void{
		this.anioActual = new Date();
		const rangoAnioActual:number = +this.anioActual.getFullYear();

		for (var i = 1990; i < rangoAnioActual+1; ++i) {
			this.aniosModeloListado.push({
				value : i,
				viewValue : i
			});
		}
	}

	validacionFechaDeAgenda(date: Date): boolean {
		this.anioActual = new Date();

		if(date == this.anioActual || date <= this.anioActual) return false;
		else return true;
	}

	checkTerminos(event: any) {
		this.check = event.currentTarget.checked;
	}

	limpiarCamposAgenda() {
		this.mostrarHoras = false;
		this.formVisibleRegistro = false;
		$(".main-panel").scrollTop(0);
		this.consultarAgenda.fechaCita = null;
		this.consultarAgenda.tecnico = null;
	}

}

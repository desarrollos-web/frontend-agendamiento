import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgendamientoComponent } from './agendamiento.component';
import { RouterModule } from '@angular/router';
import { AgendamientoRoutes } from './agendamiento.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingModule } from '../services/loading/loading.module';

@NgModule({
  declarations: [
    AgendamientoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AgendamientoRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingModule
  ]
})
export class AgendamientoModule { }

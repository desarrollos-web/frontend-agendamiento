import { Routes } from '@angular/router';

import { AgendamientoComponent } from './agendamiento.component';

export const AgendamientoRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: AgendamientoComponent
    }]
}
];

import { Routes } from '@angular/router';

import { ListarProcesosComponent } from './listar-procesos/listar-procesos.component';

export const ColisionRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: ListarProcesosComponent
    }]
}
];

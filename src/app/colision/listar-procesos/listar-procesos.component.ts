import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ColisionService } from '../../services/colision/colision.service';
import { SweetAlertService } from '../../services/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-listar-procesos',
	templateUrl: './listar-procesos.component.html',
	styleUrls: ['./listar-procesos.component.css']
})
export class ListarProcesosComponent implements OnInit {
	public loading = false;
	public visualizarProcesosColision = false;
	public buscarVehiculoPorPlaca: FormGroup;
	public procesosFinalizados: any[] = [];
	public procesosPendientes: any[] = [];
	public noExistenRegistrosVehiculo = false;
	public ocultarFormConsulta = true;
	public messageNoExistenRegistrosVehiculo: string;
	public visualizarPlaca: any;

	constructor(
		private _colisionService: ColisionService,
		private _sweetService: SweetAlertService
	) {		
		this.buscarVehiculoPorPlaca = new FormGroup({
			inpPlaca: new FormControl(null, [Validators.required, Validators.pattern("^[A-Z]{3}[0-9]{3}$")])
		});
	}

	ngOnInit(): void {
		this.loading = true;
		setTimeout(() => {
			this.loading = false;
		}, 200);
	 }

	async listarEstadoProceso() {
		this.loading = true;
		this.visualizarPlaca = this.buscarVehiculoPorPlaca.value.inpPlaca;
		await new Promise((resolve, reject) => {
			this._colisionService.listarEstadoProcesos(this.buscarVehiculoPorPlaca.value.inpPlaca).subscribe(
			(response: any) => {
				this.loading = false;
				console.log(response);
				
				this.procesosFinalizados = response.data.colisionesFinalizadas;
				this.procesosPendientes = response.data.colisionesPendientes;

				if (response.status == "success" && response.code == 200) {
					this.visualizarProcesosColision = true;
					if (this.procesosFinalizados === undefined && this.procesosPendientes === undefined || response.data == 10) {
						this.ocultarFormConsulta = false;
						this.noExistenRegistrosVehiculo = true;
						this.procesosFinalizados = null;
						this.procesosPendientes = null;
						this.messageNoExistenRegistrosVehiculo = response.message;
						this.buscarVehiculoPorPlaca.reset();
						console.log(response.message);
					} else {
						this.noExistenRegistrosVehiculo = false;
						this.ocultarFormConsulta = false;
						this.buscarVehiculoPorPlaca.reset();
					}
				} else {
					this.visualizarProcesosColision = false;
					this.procesosFinalizados = null;
					this.procesosPendientes = null;
					this._sweetService.alertGeneral('Consulta no encontrada',
						response.message,
						'Ok',
						'warning'
					);
					this.buscarVehiculoPorPlaca.reset();
				}
			}),
			(error: any) => {
				this.loading = false;
				console.log("[CONSULTA DE COLISIONES]: ", error);
				reject(error);
			};
		}).catch((error: any) => {
			this.loading = false;
			console.log(error);
			throw error;
		});
	}

	limpiarCamposConsulta() {
		this.ocultarFormConsulta = true;
		$(".main-panel").scrollTop(0);
		this.procesosFinalizados = null;
		this.procesosPendientes = null;
		this.visualizarProcesosColision = false;
	}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarProcesosComponent } from './listar-procesos.component';

describe('ListarProcesosComponent', () => {
  let component: ListarProcesosComponent;
  let fixture: ComponentFixture<ListarProcesosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarProcesosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarProcesosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarProcesosComponent } from './listar-procesos/listar-procesos.component';
import { RouterModule } from '@angular/router';
import { ColisionRoutes } from './colision.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingModule } from '../services/loading/loading.module';


@NgModule({
  declarations: [ListarProcesosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ColisionRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingModule
  ]
})
export class ColisionModule { }
